#include "system.h"

#include "dcConverter.h"

#include "communication.h"

#include "movement.h"
#include "stateMachine.h"

#include "motorControl.h"

int main(void)
{
    Communication_init();
    Movement_init();

    DCconverter_enable(true);

    StateMachine_start(STATE_MACHINE_WAIT_FOR_START);
}
