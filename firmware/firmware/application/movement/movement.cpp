#include "movement.h"

#include "system.h"

#include "motorControl.h"
#include "position.h"

struct Movement_typeDef_t
{
    int8_t targetLinerSpeed;
    int8_t targetRotationSpeed;

    PID pidPower;
    PID pidAngle;

    float angleOffset;
};

static Movement_typeDef_t movement_typeDef;

static void Movement_syncLoop(void)
{
    Euler_t euler;
    float power;

    Position_getPosition(&euler);

    power = movement_typeDef.pidPower.step(euler.roll - movement_typeDef.angleOffset);

    MotorControl_setPower(MOTOR_CONTROL_MOTOR_LEFT, power);
    MotorControl_setPower(MOTOR_CONTROL_MOTOR_RIGHT, power);
}

void Movement_init(void)
{
    movement_typeDef.pidPower.setPID(MOVEMENT_PID_KP,
                                     MOVEMENT_PID_KI,
                                     MOVEMENT_PID_KD);
    movement_typeDef.pidPower.setMinMax(-1.0f,1.0f);
    movement_typeDef.pidPower.setWindup(1.0f);

    movement_typeDef.angleOffset = 0.05;

    MotorControl_init();
    Position_init();

    system_syncFun_addFun(Movement_syncLoop, SYSTEM_SYNC_LOOP_LOW);
}

void Movement_setAngleOffset(float offset)
{
    movement_typeDef.angleOffset = offset;
}

PID* Movement_PIDpower(void)
{
    return &movement_typeDef.pidPower;
}
PID* Movement_PIDangle(void)
{
    return &movement_typeDef.pidAngle;
}

void Movement_enable(bool enable)
{
    MotorControl_enable(enable);
}
