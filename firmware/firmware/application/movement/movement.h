#ifndef INCLUDE_MOVEMENT_H
#define INCLUDE_MOVEMENT_H

#define MOVEMENT_PID_KP   30.0f
#define MOVEMENT_PID_KI   0.0f
#define MOVEMENT_PID_KD   300.0f

#include <cstdint>

#include "PID.h"

void Movement_init(void);

void Movement_setTargetSpeed(int8_t linear, int8_t rotation);

void Movement_enable(bool enable);

void Movement_setAngleOffset(float offset);

PID* Movement_PIDpower(void);
PID* Movement_PIDangle(void);

#endif
