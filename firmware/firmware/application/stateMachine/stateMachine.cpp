#include "stateMachine.h"

void StateMachine_start(StateMachine_stateID_e startState)
{
    StateMachine_stateID_e currentState = startState;

    while (currentState != __STATE_MACHINE_STATE_EXIT__)
    {
        stateMachine_states[currentState].handler();
        currentState = stateMachine_states[currentState].nextStateID;
    }
}
