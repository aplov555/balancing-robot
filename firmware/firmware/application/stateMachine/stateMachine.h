#ifndef INCLUDE_STATE_MACHINE_H
#define INCLUDE_STATE_MACHINE_H

#include "stateMachine_states.h"

void StateMachine_start(StateMachine_stateID_e state);

#endif
