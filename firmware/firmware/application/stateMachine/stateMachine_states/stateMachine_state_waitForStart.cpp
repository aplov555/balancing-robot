#include "stateMachine_states.h"

#include "system.h"

#include "signalLed.h"

#include "position.h"
#include "communication.h"
#include "bno080.h"

#include "movement.h"

#include <cmath>

int save;

void StateMachine_state_waitForStart(void)
{
    Euler_t euler;

    SignalLed_set(SIGNAL_LED_2, true);
    Movement_enable(false);

    while (true)
    {
        Position_updateData();

        Position_getPosition(&euler);
        if (fabsf(euler.roll) < 0.03f)
        {
            break;
        }

        if(save)
        {
            save = 0;

            BNO080_saveCalibration();
        }

        Communication_sendTelemetry();
    }
}
