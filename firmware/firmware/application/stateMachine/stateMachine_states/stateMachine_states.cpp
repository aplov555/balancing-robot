#include "stateMachine.h"

StateMachine_state_t stateMachine_states[] =
{
    StateMachine_state_t(StateMachine_state_waitForStart, STATE_MACHINE_BALANCING),
    StateMachine_state_t(StateMachine_state_balancing, STATE_MACHINE_WAIT_FOR_START)
};
