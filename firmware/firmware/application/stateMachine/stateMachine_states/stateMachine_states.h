#ifndef INCLUDE_STATE_MACHINE_STATES_H
#define INCLUDE_STATE_MACHINE_STATES_H

enum StateMachine_stateID_e
{
    STATE_MACHINE_WAIT_FOR_START,
    STATE_MACHINE_BALANCING,
    __STATE_MACHINE_STATE_EXIT__, // should be at the end of this enum
};

typedef void (*StateMachine_stateHandler_t)(void);

struct StateMachine_state_t
{
    StateMachine_stateHandler_t handler;
    StateMachine_stateID_e nextStateID;

    StateMachine_state_t(StateMachine_stateHandler_t handler,
            StateMachine_stateID_e nextStateID)
    {
        this->handler = handler;
        this->nextStateID = nextStateID;
    }
};

void StateMachine_state_waitForStart(void);
void StateMachine_state_balancing(void);

extern StateMachine_state_t stateMachine_states[];

#endif
