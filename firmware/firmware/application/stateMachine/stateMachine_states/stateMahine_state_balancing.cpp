#include "stateMachine_states.h"

#include "system.h"

#include "signalLed.h"

#include "position.h"
#include "communication.h"

#include "movement.h"

#include <cmath>

void StateMachine_state_balancing(void)
{
    Euler_t euler;

    SignalLed_set(SIGNAL_LED_2, false);
    Movement_enable(true);

    while (true)
    {
        Position_updateData();

        Position_getPosition(&euler);
        if (fabsf(euler.roll) > 0.7f)
        {
            break;
        }

        Communication_sendTelemetry();
    }
}
