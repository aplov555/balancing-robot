#include "communication.h"

#include "system.h"
#include "usart.h"
#include "adc.h"

#include "motorControl.h"
#include "position.h"
#include "dataPacket.h"

#include "movement.h"

#include <stdio.h>
#include <stdlib.h>

static char buff[128];
static volatile bool start;
static volatile uint8_t byte;
static volatile bool newDataReady;
static bool sendDelay;
static DataPacket_t packet[6];

void Communication_init(void)
{
    system_syncFun_addFun(Communication_processData, SYSTEM_SYNC_LOOP_LOW);
}

void Communication_sendTelemetry(void)
{
    float tmp;
    int32_t temp;
    Euler_t euler;

    if(sendDelay)
    {
        sendDelay = false;

        MotorControl_getPower(MOTOR_CONTROL_MOTOR_LEFT, &tmp);
        dataPacket_prepare("MLP", 1000 * tmp ,&packet[0]);

        MotorControl_getPower(MOTOR_CONTROL_MOTOR_RIGHT, &tmp);
        dataPacket_prepare("MRP", 1000 * tmp ,&packet[1]);

        MotorControl_getDistance(MOTOR_CONTROL_MOTOR_LEFT, &temp);
        dataPacket_prepare("MLD", temp, &packet[2]);

        MotorControl_getDistance(MOTOR_CONTROL_MOTOR_RIGHT, &temp);
        dataPacket_prepare("MRD", temp, &packet[3]);

        Position_getPosition(&euler);
        dataPacket_prepare("PY", 1000 * euler.roll, &packet[4]);

        Position_getPosition(&euler);
        dataPacket_prepare("BV", adcVal, &packet[5]);

        USART_sendPacket((uint8_t*)packet, sizeof(packet));
    }
}


void Communication_processData(void)
{
    static uint32_t cnt = 0 ;
    if(++cnt == 75 )
    {
        cnt = 0;
        sendDelay = true;
    }

    if (!newDataReady)
    {
        return;
    }

    float kp, ki, kd;
    char* end;

    switch (buff[0])
    {
    case COMMUNICATION_CHANNEL_CONFIG:

        switch (buff[1])
        {
        case 'M':

            kp = strtof(buff + 4, &end);
            ki = strtof(end, &end);
            kd = strtof(end, NULL);

            switch (buff[2])
            {
            case 'L':

                Movement_PIDpower()->setPID(kp, ki, kd);

                break;

            case 'P':

                Movement_PIDangle()->setPID(kp, ki, kd);

                break;
            }

            break;

        case 'A':

            kp = strtof(buff + 3, &end);
            ki = strtof(end, &end);
            kd = strtof(end, NULL);

            // Movement_getPIDAngle()->setPID(kp, ki, kd);

            break;

        case 'S':

            kp = strtof(buff + 3, &end);
            ki = strtof(end, &end);
            kd = strtof(end, NULL);

            // Movement_getPIDSpeed()->setPID(kp, ki, kd);

            break;

        case 'F':

            Movement_setAngleOffset(strtof(buff+3, NULL));

            break;
        }

        break;

    case COMMUNICATION_CHANNEL_SENSOR_DATA:

        break;

    case COMMUNICATION_CHANNEL_MOTION:

        break;
    }

    newDataReady = false;
}

extern "C"
{
void __attribute__((interrupt)) UART4_IRQHandler(void)
{
    GPIOB->ODR^= GPIO_ODR_ODR_6;
    if (UART4->ISR & USART_ISR_RXNE)
    {
        if (!newDataReady)
        {
            if (start)
            {
                buff[byte] = UART4->RDR;

                if (buff[byte] == '\0')
                {
                    start = false;
                    newDataReady = true;
                } else
                {
                    ++byte;
                }
            } else
            {
                if (UART4->RDR == '\n')
                {
                    start = true;
                    byte = 0;
                }
            }
        } else
        {
            UART4->RDR;
        }
    }else
    {
        UART4->ICR = UINT32_MAX;
    }
}
}
