#ifndef INCLUDE_COMMUNICATION_H
#define INCLUDE_COMMUNICATION_H

#include <cstdint>

enum Communication_channel_e
{
    COMMUNICATION_CHANNEL_CONFIG = 'C',
    COMMUNICATION_CHANNEL_SENSOR_DATA = 'D',
    COMMUNICATION_CHANNEL_MOTION = 'M',
};

void Communication_init(void);

void Communication_processData(void);
void Communication_sendTelemetry(void);

bool Communication_sendData(uint8_t* buff, uint16_t len);

#endif
