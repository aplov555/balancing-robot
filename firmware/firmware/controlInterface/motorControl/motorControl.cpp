#include "motorControl.h"

#include "system.h"

#include "drv8313.h"
#include "as5048a.h"

#include "svpwm.h"

struct MotorControl_typeDef_t
{
    SVPWM_timing_t timing;
    float power;

    uint16_t angle;
    uint16_t anglePrev;
    int8_t speed;
    int32_t distance;
};

static MotorControl_typeDef_t motorControl_typeDef[2];

static void MotorControl_computeMotorAngle(MotorControl_motor_e motor)
{
    switch (motor)
    {
    case MOTOR_CONTROL_MOTOR_LEFT:

        motorControl_typeDef[motor].angle = AS5048A_getAngle(AS5048A_ENCODER_LEFT) + MOTOR_CONTROL_MOTOR_LEFT_ENCODER_OFFSET;

        break;

    case MOTOR_CONTROL_MOTOR_RIGHT:

        motorControl_typeDef[motor].angle = AS5048A_DATA_CLOCKS - AS5048A_getAngle(AS5048A_ENCODER_RIGHT) + MOTOR_CONTROL_MOTOR_RIGHT_ENCODER_OFFSET;

        break;
    }
}

static void MotorControl_computeSpeed(MotorControl_motor_e motor)
{
    int16_t delta = motorControl_typeDef[motor].angle - motorControl_typeDef[motor].anglePrev;

    if(delta < 100 && delta > -100)
    {
        motorControl_typeDef[motor].speed = delta;
        motorControl_typeDef[motor].distance += delta;
    }

    motorControl_typeDef[motor].anglePrev = motorControl_typeDef[motor].angle;
}

static void MotorControl_computeTiming(MotorControl_motor_e motor)
{
    uint32_t SVPWMangle;
    float power = motorControl_typeDef[motor].power;

    MotorControl_computeMotorAngle(motor);
    MotorControl_computeSpeed(motor);

    if (power > 0)
    {
        SVPWMangle = motorControl_typeDef[motor].angle + ((SVPWM_MAX_ANGLE+1) >> 2);
    } else
    {
        SVPWMangle = motorControl_typeDef[motor].angle - ((SVPWM_MAX_ANGLE+1) >> 2);
        power = -power;
    }

    SVPWMangle *= MOTOR_CONTROL_MOTOR_ANG_TO_SVPWM_ANG;
    SVPWMangle = SVPWMangle % (SVPWM_MAX_ANGLE + 1);

    SVPWM_computeTiming(SVPWMangle, power, &motorControl_typeDef[motor].timing);
}

static void MotorControl_syncLoop(void)
{
    MotorControl_computeTiming(MOTOR_CONTROL_MOTOR_LEFT);
    MotorControl_computeTiming(MOTOR_CONTROL_MOTOR_RIGHT);

    DRV8313_setTiming(DRV8313_DRIVER_LEFT,
            (DRV8313_timing_t*) &motorControl_typeDef[MOTOR_CONTROL_MOTOR_LEFT].timing);
    DRV8313_setTiming(DRV8313_DRIVER_RIGHT,
            (DRV8313_timing_t*) &motorControl_typeDef[MOTOR_CONTROL_MOTOR_RIGHT].timing);
}

void MotorControl_init(void)
{
    SVPWM_init();

    MotorControl_enable(true);

    system_syncFun_addFun(MotorControl_syncLoop, SYSTEM_SYNC_LOOP_HIGH);
}

void MotorControl_setPower(MotorControl_motor_e motor, float power)
{
    motorControl_typeDef[motor].power = power;
}

void MotorControl_enable(bool enable)
{
    DRV8313_enable(DRV8313_DRIVER_LEFT, enable);
    DRV8313_enable(DRV8313_DRIVER_RIGHT, enable);
}

void MotorControl_getSpeed(MotorControl_motor_e motor, int8_t* speed)
{
    *speed = motorControl_typeDef[motor].speed;
}

void MotorControl_getPower(MotorControl_motor_e motor, float* power)
{
    *power = motorControl_typeDef[motor].power;
}

void MotorControl_getDistance(MotorControl_motor_e motor, int32_t* distance)
{
    *distance = motorControl_typeDef[motor].distance;
}
