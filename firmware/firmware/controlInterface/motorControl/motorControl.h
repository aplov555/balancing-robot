#ifndef INCLUDE_MOTOR_CONTROL_H
#define INCLUDE_MOTOR_CONTROL_H

#define MOTOR_CONTROL_MOTOR_ANG_TO_SVPWM_ANG	    2.75f

#define MOTOR_CONTROL_MOTOR_LEFT_ENCODER_OFFSET     923
#define MOTOR_CONTROL_MOTOR_RIGHT_ENCODER_OFFSET    311

#include <cstdint>

#include "pid.h"

enum MotorControl_motor_e
{
    MOTOR_CONTROL_MOTOR_LEFT = 0,
    MOTOR_CONTROL_MOTOR_RIGHT = 1,
};

enum MotorControl_status_e
{
    MOTOR_CONTROL_STATUS_ERROR = 0,
    MOTOR_CONTROL_STATUS_OK = 1,
    MOTOR_CONTROL_STATUS_FUSE = 2,
};

void MotorControl_init(void);

void MotorControl_setPower(MotorControl_motor_e motor, float power);

PID* MotorControl_pid(MotorControl_motor_e motor);

void MotorControl_enable(bool enable);

void MotorControl_getPower(MotorControl_motor_e motor, float* power);
void MotorControl_getEncoder(MotorControl_motor_e motor, uint16_t* enc);
void MotorControl_getStatus(MotorControl_motor_e motor, MotorControl_status_e* status);
void MotorControl_getDistance(MotorControl_motor_e motor, int32_t* distance);
void MotorControl_getSpeed(MotorControl_motor_e motor, int8_t* speed);

#endif
