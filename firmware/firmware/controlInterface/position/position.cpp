#include "position.h"

#include "system.h"
#include "arm_math.h"

#include "bno080.h"

#include "MadgwickAHRS.h"

#include <cmath>

#define M_PI_2      1.5707963267948966192313216916398f
#define M_PI        3.1415926535897932384626433832795f

Euler_t euler;
float xyz[3];

static void Position_quatToEuer(float* q, Euler_t* e)
{
    float sinr_cosp = 2 * (q[3] * q[0] + q[1] * q[2]);
    float cosr_cosp = 1 - 2 * (q[0] * q[0] + q[1] * q[1]);
    e->roll = atan2f(sinr_cosp, cosr_cosp);

    float sinp = 2 * (q[3] * q[1] - q[2] * q[0]);
    if (std::abs(sinp) >= 1)
        e->pitch = std::copysign(M_PI / 2, sinp); // use 90 degrees if out of range
    else
        e->pitch = std::asin(sinp);

    float siny_cosp = 2 * (q[3] * q[2] + q[0] * q[1]);
    float cosy_cosp = 1 - 2 * (q[1] * q[1] + q[2] * q[2]);
    e->yaw = atan2f(siny_cosp, cosy_cosp);

}

static void qToFloat(int16_t* fixedPointValue, uint8_t len, uint8_t qPoint, float* output)
{
    float mul = powf(2, qPoint * -1);

    for(uint8_t q = 0; q < len; ++q)
    {
        output[q] = fixedPointValue[q] * mul;
    }
}

BNO080_sensorData_t sensorData;

void Position_updateData(void)
{
    SH2_sensorID_e sensorID;

    if (BNO080_getSensorData(&sensorData, &sensorID))
    {
        if(sensorID == SH2_ARVR_STABILIZED_GRV)
        {
            float temp[4];
            qToFloat(sensorData.data, 4, 14, temp);

            Position_quatToEuer(temp, &euler);
        }

        if(sensorID == SH2_ACCELEROMETER)
        {
            qToFloat(sensorData.data, 3, 8, xyz);
        }
    }
}

void Position_init(void)
{
    BNO080_init();
    BNO080_calibrate();
    BNO080_enableSensor(SH2_ARVR_STABILIZED_GRV, 2500, 0);
    BNO080_enableSensor(SH2_ACCELEROMETER, 250000, 0);
}

void Position_getPosition(Euler_t* e)
{
    *e = euler;
}
