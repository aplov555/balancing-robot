#ifndef INCLUDE_POSITION_H
#define INCLUDE_POSITION_H

#include <cstdint>

struct Euler_t
{
    float pitch;
    float yaw;
    float roll;
};

void Position_init(void);

void Position_updateData(void);

void Position_getPosition(Euler_t* e);

#endif
