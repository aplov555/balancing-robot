#include "dataPacket.h"

#include <stdio.h>
#include <cstring>

static uint16_t crc16(const char* data, uint8_t len)
{
    char x;
    uint16_t crc = 0xFFFF;

    while (len--)
    {
        x = crc >> 8 ^ *data++;
        x ^= x>>4;
        crc = (crc << 8) ^ ((uint16_t)(x << 12)) ^ ((uint16_t)(x <<5)) ^ ((uint16_t)x);
    }
    return crc;
}

void dataPacket_prepare(const char* header, const char* data, DataPacket_t* packet)
{
    memset(packet, 0, sizeof(DataPacket_t));

    packet->startByte = '\n';
    strcpy((char*)packet->header, header);
    strcpy((char*)packet->data, data);
    sprintf(packet->CRC16, "%d", crc16(packet->data, 16));
}


void dataPacket_prepare(const char* header, const int32_t data, DataPacket_t* packet)
{
    char buff[16];

    sprintf(buff, "%ld", data);
    dataPacket_prepare(header, buff, packet);
}
