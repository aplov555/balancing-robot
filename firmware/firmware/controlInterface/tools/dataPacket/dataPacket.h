#ifndef INCLUDE_DATA_PACKET_H
#define INCLUDE_DATA_PACKET_H

#include <cstdint>

struct DataPacket_t
{
    char startByte;
    char header[4];
    char data[16];
    char CRC16[6];
};

void dataPacket_prepare(const char* header, const char* data, DataPacket_t* packet);
void dataPacket_prepare(const char* header, const int32_t data, DataPacket_t* packet);

#endif
