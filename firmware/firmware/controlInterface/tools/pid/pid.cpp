#include "pid.h"

PID::PID()
{
    p = i = d = 0;
    kP = kI = kD = 0;
    errorPrev = 0;
    min = -10e10;
    max = 10e10;
    windup = 10e10;
}

float PID::step(float error)
{
    float output;

    p = kP * error;

    i += kI * error;
    if (i > windup)
    {
        i = windup;
    } else if (i < -windup)
    {
        i = -windup;
    }

    d = kD * (error - errorPrev);
    errorPrev = error;

    output = p + i + d;
    if (output > max)
    {
        output = max;
    } else if (output < min)
    {
        output = min;
    }

    return output;
}

void PID::setPID(float kP, float kI, float kD)
{
    this->kP = kP;
    this->kI = kI;
    this->kD = kD;
}

void PID::setWindup(float windup)
{
    this->windup = windup;
}

void PID::setMinMax(float min, float max)
{
    this->min = min;
    this->max = max;
}


void PID::multiplyIntegral(float multipler)
{
    i *= multipler;
}

void PID::resetIntegral(void)
{
    i = 0;
}
