#ifndef INCLUDE_PID_H
#define INCLUDE_PID_H

class PID
{
public:
    PID();

    float step(float error);

    void setPID(float kP, float kI, float kD);
    void setWindup(float windup);
    void setMinMax(float min, float max);

    void multiplyIntegral(float multipler);
    void resetIntegral(void);

private:
    float kP;
    float kI;
    float kD;

    float p;
    float i;
    float d;

    float errorPrev;

    float windup;
    float min;
    float max;
};

#endif
