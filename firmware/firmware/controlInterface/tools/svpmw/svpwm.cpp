#include "svpwm.h"

#include "arm_math.h"

#define SVPWM_SECTORS           6
#define SVPWM_PHASES            3
#define SVPWM_PI                3.1415926535897932384626433832795f
#define SVPWM_PI_2              SVPWM_PI / 2
#define SVPWM_PI_3              SVPWM_PI / 3


#define SVPWM_ANGLE_TO_SECTOR   SVPWM_SECTORS / (SVPWM_MAX_ANGLE+1)
#define SVPWM_ANGLE_TO_RADIAN   2 * SVPWM_PI / SVPWM_MAX_ANGLE

static uint16_t SVPWM_SINE_LOOKUP[SVPWM_MAX_ANGLE+1];

static const uint8_t SVPWM_TIMING_SELECTOR[SVPWM_SECTORS][SVPWM_PHASES] =
{
    { 0, 2, 3 }, // Sector 1
    { 1, 0, 3 }, // Sector 2
    { 3, 0, 2 }, // Sector 3
    { 3, 1, 0 }, // Sector 4
    { 2, 3, 0 }, // Sector 5
    { 0, 3, 1 }, // Sector 6
};

void SVPWM_init(void)
{
    for(uint16_t q = 0; q < SVPWM_MAX_ANGLE+1; ++q)
    {
        SVPWM_SINE_LOOKUP[q] = SVPWM_MAX_PWM * arm_sin_f32(SVPWM_PI * q / (SVPWM_MAX_ANGLE+1));
    }
}

void SVPWM_computeTiming(uint16_t angle, float amplitude,
        SVPWM_timing_t* timing)
{
    const uint8_t sector = (uint32_t) angle * SVPWM_ANGLE_TO_SECTOR;

    const uint16_t argT1 = (float) SVPWM_MAX_ANGLE / 6 * (sector + 1) - angle;
    const uint16_t argT2 = angle - (float) SVPWM_MAX_ANGLE / 6 * sector;

    const uint16_t T1 = amplitude * SVPWM_SINE_LOOKUP[argT1];
    const uint16_t T2 = amplitude * SVPWM_SINE_LOOKUP[argT2];
    const uint16_t halfT0 = (SVPWM_MAX_PWM - T1 - T2) / 2;

    const uint16_t timings[] =
    { uint16_t(T1 + T2 + halfT0), uint16_t(T1 + halfT0), uint16_t(T2 + halfT0),
            halfT0 };

    timing->phase_A = timings[SVPWM_TIMING_SELECTOR[sector][0]];
    timing->phase_B = timings[SVPWM_TIMING_SELECTOR[sector][1]];
    timing->phase_C = timings[SVPWM_TIMING_SELECTOR[sector][2]];
}
