#ifndef SVPWM_INCLUDE
#define SVPWM_INCLUDE

#include "system.h"

#define SVPWM_MAX_PWM        SYSTEM_MOTORS_PWM_MAX
#define SVPWM_MAX_ANGLE      1023

typedef struct SVPWM_timing_t
{
    uint16_t phase_A;
    uint16_t phase_B;
    uint16_t phase_C;
} SVPWM_timing_t;

void SVPWM_init(void);

void SVPWM_computeTiming(uint16_t angle, float amplitude,
        SVPWM_timing_t* timing);

#endif
