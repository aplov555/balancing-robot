#include "dcConverter.h"

#include "system.h"

void DCconverter_enable(bool enable)
{
    if (enable)
    {
        GPIOA->BSRR = GPIO_BSRR_BS_2;
    } else
    {
        GPIOA->BSRR = GPIO_BSRR_BR_2;
    }
}
