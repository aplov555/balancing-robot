#include "as5048a.h"

#include "stm32f722xx.h"

#define AS5048A_ENCODER_LEFT_PERIOD    (TIM9->CCR2)
#define AS5048A_ENCODER_LEFT_DUTY      (TIM9->CCR1)

#define AS5048A_ENCODER_RIGHT_PERIOD    (TIM2->CCR1)
#define AS5048A_ENCODER_RIGHT_DUTY      (TIM2->CCR2)

#define AS5048A_TOTAL_CLOCKS            (AS5048A_INIT_CLOCKS + AS5048A_DATA_CLOCKS + AS5048A_EXIT_CLOCKS)
#define AS5048A_HALF_DATA_CLOCKS        (AS5048A_DATA_CLOCKS >> 1)

int16_t AS5048A_getAngle(AS5048A_encoder_e encoder)
{
    int16_t angle;

    switch (encoder)
    {
    case AS5048A_ENCODER_LEFT:

        angle = AS5048A_TOTAL_CLOCKS * AS5048A_ENCODER_LEFT_DUTY / AS5048A_ENCODER_LEFT_PERIOD;

        break;
    case AS5048A_ENCODER_RIGHT:

        angle = AS5048A_TOTAL_CLOCKS * AS5048A_ENCODER_RIGHT_DUTY / AS5048A_ENCODER_RIGHT_PERIOD;

        break;
    default:

        return 0;
    }
    angle -= AS5048A_INIT_CLOCKS;

    if (angle < 0)
    {
        angle = 0;
    } else if (angle > AS5048A_DATA_CLOCKS)
    {
        angle = AS5048A_DATA_CLOCKS;
    }

    return angle;
}
