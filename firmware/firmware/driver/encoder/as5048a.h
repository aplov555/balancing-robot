#ifndef INCLUDE_AS5048A_H
#define INCLUDE_AS5048A_H

#define AS5048A_INIT_CLOCKS     16
#define AS5048A_DATA_CLOCKS     4095
#define AS5048A_EXIT_CLOCKS     8

#include <cstdint>

enum AS5048A_encoder_e
{
    AS5048A_ENCODER_LEFT, AS5048A_ENCODER_RIGHT
};

int16_t AS5048A_getAngle(AS5048A_encoder_e encoder);

#endif
