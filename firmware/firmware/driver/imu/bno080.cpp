#include "bno080.h"
#include "shtp-sh2.h"

#include "system.h"
#include "spi.h"

struct BNO080_typeDef_t
{
    SHTP_packed_t packet;
};
static BNO080_typeDef_t BNO080_typeDef;

static bool BNO080_getInt(void)
{
    return GPIOC->IDR & GPIO_IDR_IDR_4;
}

static bool BNO080_waitForInt(uint16_t ms)
{
    uint32_t tick = system_get_systemTick();

    while (GPIOC->IDR & GPIO_IDR_IDR_4)
    {
        if (system_get_systemTick() - tick >= ms)
        {
            return false;
        }
    }

    return true;
}

static bool BNO080_reset(void)
{
    GPIOB->BSRR = GPIO_BSRR_BR_0;
    system_delayms(10);
    GPIOB->BSRR = GPIO_BSRR_BS_0;

    return BNO080_waitForInt(UINT8_MAX);
}

static bool BNO080_receivePacket(void)
{
    uint16_t bytesToRead;
    uint8_t buffer;
    bool result = true;

    BNO080_waitForInt(UINT8_MAX);
    SPI_chipSelect(true);

    SPI_read(BNO080_typeDef.packet.header, SHTP_PACKED_HEADER_SIZE);
    if (BNO080_typeDef.packet.header[0] != UINT8_MAX &&
        BNO080_typeDef.packet.header[1] != UINT8_MAX)
    {
        bytesToRead = (BNO080_typeDef.packet.header[1] << 8) | BNO080_typeDef.packet.header[0];
        bytesToRead &= ~(1 << 15);

        if (bytesToRead != 0)
        {
            bytesToRead -= SHTP_PACKED_HEADER_SIZE;

            for (uint16_t q = 0; q < bytesToRead; ++q)
            {
                buffer = SPI_sendReadByte(UINT8_MAX);

                if (q < SHTP_PACKED_BUFFER_SIZE)
                {
                    BNO080_typeDef.packet.data[q] = buffer;
                }
            }
        } else
        {
            result = false;
        }
    } else
    {
        result = false;
    }

    SPI_chipSelect(false);

    return result;
}
static bool BNO080_sendPacket(SHTP_channel_e channel, uint8_t dataLen)
{
    uint16_t packetLen = dataLen + SHTP_PACKED_HEADER_SIZE;

    BNO080_waitForInt(UINT8_MAX);
    SPI_chipSelect(true);

    SPI_sendReadByte(packetLen & UINT8_MAX);
    SPI_sendReadByte(packetLen >> 8);
    SPI_sendReadByte(channel);
    SPI_sendReadByte(BNO080_typeDef.packet.sequenceNumber[channel]++);

    SPI_send(BNO080_typeDef.packet.data, dataLen);

    SPI_chipSelect(false);

    return true;
}

static bool BNO080_sendCommand(SH2_command_e command)
{
    BNO080_typeDef.packet.data[0] = SHTP_REPORT_COMMAND_REQUEST;
    BNO080_typeDef.packet.data[1] =
            BNO080_typeDef.packet.commandSequenceNumber++;
    BNO080_typeDef.packet.data[2] = command;

    return BNO080_sendPacket(SHTP_CHANNEL_CONTROL, 12);
}

static bool BNO080_checkProductID(void)
{
    BNO080_typeDef.packet.data[0] = SHTP_REPORT_PRODUCT_ID_REQUEST;
    BNO080_typeDef.packet.data[1] = 0;
    BNO080_sendPacket(SHTP_CHANNEL_CONTROL, 2);

    BNO080_receivePacket();

    return BNO080_typeDef.packet.data[0] == SHTP_REPORT_PRODUCT_ID_RESPONSE;
}

static void BNO080_parseInputReport(BNO080_sensorData_t* sensorData,
        SH2_sensorID_e* sensorID)
{
    uint16_t dataLen = (BNO080_typeDef.packet.header[1] << 8) | BNO080_typeDef.packet.header[0];
    dataLen &= ~(1 << 15);
    dataLen -= 4;

    if (BNO080_typeDef.packet.data[5] <= SH2_MAX_SENSOR_ID)
    {
        *sensorID = SH2_sensorID_e(BNO080_typeDef.packet.data[5]);

        sensorData->status = SH2_sensorStatus_e(BNO080_typeDef.packet.data[7] & 0x03);
        sensorData->data[0] = int16_t((BNO080_typeDef.packet.data[10] << 8) | BNO080_typeDef.packet.data[9]);
        sensorData->data[1] = int16_t((BNO080_typeDef.packet.data[12] << 8) | BNO080_typeDef.packet.data[11]);
        sensorData->data[2] = int16_t((BNO080_typeDef.packet.data[14] << 8) | BNO080_typeDef.packet.data[13]);
        sensorData->data[3] = dataLen > 14 ? int16_t((BNO080_typeDef.packet.data[16] << 8) | BNO080_typeDef.packet.data[15]) : 0;
        sensorData->data[4] = dataLen > 16 ? int16_t((BNO080_typeDef.packet.data[18] << 8) | BNO080_typeDef.packet.data[17]) : 0;
    }
}
static void BNO080_parseCmdReport(void)
{

}

bool BNO080_init(void)
{
    GPIOB->BSRR = GPIO_BSRR_BS_2; // BOOT
    GPIOB->BSRR = GPIO_BSRR_BS_1; // PS0/WAKE

    for (uint8_t q = 0; q < 5; ++q)
    {
        if (BNO080_reset())
        {
            BNO080_receivePacket();     // Listen ad.
            BNO080_receivePacket();     // Read init response

            if (BNO080_checkProductID())
            {
                return true;
            }
        }
    }

    return false;
}

bool BNO080_enableSensor(SH2_sensorID_e sensorID, uint32_t intervalReportsUs,
        uint32_t specificConfig)
{
    BNO080_typeDef.packet.data[0] = SHTP_REPORT_SET_FEATURE_COMMAND;
    BNO080_typeDef.packet.data[1] = sensorID;
    BNO080_typeDef.packet.data[2] = 0;
    BNO080_typeDef.packet.data[3] = 0;
    BNO080_typeDef.packet.data[4] = 0;
    BNO080_typeDef.packet.data[5] = (intervalReportsUs >> 0) & UINT8_MAX;
    BNO080_typeDef.packet.data[6] = (intervalReportsUs >> 8) & UINT8_MAX;
    BNO080_typeDef.packet.data[7] = (intervalReportsUs >> 16) & UINT8_MAX;
    BNO080_typeDef.packet.data[8] = (intervalReportsUs >> 24) & UINT8_MAX;
    BNO080_typeDef.packet.data[9] = 0;
    BNO080_typeDef.packet.data[10] = 0;
    BNO080_typeDef.packet.data[11] = 0;
    BNO080_typeDef.packet.data[12] = 0;
    BNO080_typeDef.packet.data[13] = (specificConfig >> 0) & UINT8_MAX;
    BNO080_typeDef.packet.data[14] = (specificConfig >> 8) & UINT8_MAX;
    BNO080_typeDef.packet.data[15] = (specificConfig >> 16) & UINT8_MAX;
    BNO080_typeDef.packet.data[16] = (specificConfig >> 24) & UINT8_MAX;

    return BNO080_sendPacket(SHTP_CHANNEL_CONTROL, 17);
}

bool BNO080_getSensorData(BNO080_sensorData_t* sensorData,
        SH2_sensorID_e* sensorID)
{
    *sensorID = SH2_sensorID_e(0);

    if (!BNO080_getInt())
    {
        if (BNO080_receivePacket())
        {
            if (BNO080_typeDef.packet.header[2] == SHTP_CHANNEL_REPORTS &&
                BNO080_typeDef.packet.data[0] == SHTP_REPORT_BASE_TIMESTAMP)
            {
                BNO080_parseInputReport(sensorData, sensorID);

                return true;
            }
            else if(BNO080_typeDef.packet.header[2] == SHTP_CHANNEL_CONTROL)
            {
                BNO080_parseCmdReport();

                return true;
            }
            else if(BNO080_typeDef.packet.header[2] == SHTP_CHANNEL_GYR)
            {
                BNO080_parseInputReport(sensorData, sensorID);

                return true;
            }
        }
    }

    return false;
}

bool BNO080_calibrate(void)
{
    BNO080_typeDef.packet.data[3] = 1;
    BNO080_typeDef.packet.data[4] = 1;
    BNO080_typeDef.packet.data[5] = 1;
    BNO080_typeDef.packet.data[6] = 0;
    BNO080_typeDef.packet.data[7] = 0;
    BNO080_typeDef.packet.data[8] = 0;
    BNO080_typeDef.packet.data[9] = 0;
    BNO080_typeDef.packet.data[10] = 0;
    BNO080_typeDef.packet.data[11] = 0;

    return BNO080_sendCommand(SH2_COMMAND_ME_CALIBRATE);
}

bool BNO080_saveCalibration(void)
{
    BNO080_typeDef.packet.data[3] = 0;
    BNO080_typeDef.packet.data[4] = 0;
    BNO080_typeDef.packet.data[5] = 0;
    BNO080_typeDef.packet.data[6] = 0;
    BNO080_typeDef.packet.data[7] = 0;
    BNO080_typeDef.packet.data[8] = 0;
    BNO080_typeDef.packet.data[9] = 0;
    BNO080_typeDef.packet.data[10] = 0;
    BNO080_typeDef.packet.data[11] = 0;

    BNO080_sendCommand(SH2_COMMAND_DCD);
}

bool BNO080_tare(SH2_sensorID_e sensor)
{
    BNO080_typeDef.packet.data[3] = 0;
    BNO080_typeDef.packet.data[4] = 0;
    BNO080_typeDef.packet.data[6] = 1;
    BNO080_typeDef.packet.data[7] = 0;
    BNO080_typeDef.packet.data[8] = 0;
    BNO080_typeDef.packet.data[9] = 0;
    BNO080_typeDef.packet.data[10] = 0;
    BNO080_typeDef.packet.data[11] = 0;

    return BNO080_sendCommand(SH2_COMMAND_ME_CALIBRATE);
}
