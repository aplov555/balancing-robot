#ifndef INCLUDE_BNO080_H
#define INCLUDE_BNO080_H

#include <cstdint>

#include "shtp-sh2.h"

struct BNO080_sensorData_t
{
    SH2_sensorStatus_e status;
    int16_t data[5];
};

bool BNO080_init(void);

bool BNO080_enableSensor(SH2_sensorID_e sensorID, uint32_t intervalReportsUs,
        uint32_t specificConfig);

bool BNO080_getSensorData(BNO080_sensorData_t* sensorData,
        SH2_sensorID_e* sensorID);

bool BNO080_calibrate(void);
bool BNO080_saveCalibration(void);
bool BNO080_tare(SH2_sensorID_e sensor);

#endif
