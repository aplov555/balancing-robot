#include "signalLed.h"

#include "system.h"

void SignalLed_set(SignalLed_led_e led, bool set)
{
    switch (led)
    {
    case SIGNAL_LED_1:

        if (set)
        {
            GPIOB->BSRR = GPIO_BSRR_BS_6;
        } else
        {
            GPIOB->BSRR = GPIO_BSRR_BR_6;
        }

        break;

    case SIGNAL_LED_2:

        if (set)
        {
            GPIOB->BSRR = GPIO_BSRR_BS_7;
        } else
        {
            GPIOB->BSRR = GPIO_BSRR_BR_7;
        }

        break;

    case SIGNAL_LED_3:

        if (set)
        {
            GPIOB->BSRR = GPIO_BSRR_BS_8;
        } else
        {
            GPIOB->BSRR = GPIO_BSRR_BR_8;
        }

        break;
    }
}
