#ifndef INCLUDE_SIGNAL_LED_H
#define INCLUDE_SIGNAL_LED_H

enum SignalLed_led_e
{
    SIGNAL_LED_1, SIGNAL_LED_2, SIGNAL_LED_3,
};

void SignalLed_set(SignalLed_led_e led, bool set);

#endif
