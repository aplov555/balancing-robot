#include "drv8313.h"

#include "system.h"

void DRV8313_enable(DRV8313_driver_e driver, bool enable)
{
    switch (driver)
    {
    case DRV8313_DRIVER_LEFT:

        if (enable)
        {
            GPIOB->BSRR = GPIO_BSRR_BS_11 |
                          GPIO_BSRR_BS_12 |
                          GPIO_BSRR_BS_13;
            GPIOC->BSRR = GPIO_BSRR_BS_6 |
                          GPIO_BSRR_BS_8;
        } else
        {
            GPIOB->BSRR = GPIO_BSRR_BR_11 |
                          GPIO_BSRR_BR_12 |
                          GPIO_BSRR_BR_13;
            GPIOC->BSRR = GPIO_BSRR_BR_6 |
                          GPIO_BSRR_BR_8;
        }

        break;

    case DRV8313_DRIVER_RIGHT:

        if (enable)
        {
            GPIOA->BSRR = GPIO_BSRR_BS_8 |
                          GPIO_BSRR_BS_10 |
                          GPIO_BSRR_BS_12;
            GPIOC->BSRR = GPIO_BSRR_BS_10 |
                          GPIO_BSRR_BS_11;
        } else
        {
            GPIOA->BSRR = GPIO_BSRR_BR_8 |
                          GPIO_BSRR_BR_10 |
                          GPIO_BSRR_BR_12;
            GPIOC->BSRR = GPIO_BSRR_BR_10 |
                          GPIO_BSRR_BR_11;
        }

        break;
    }
}

void DRV8313_setTiming(DRV8313_driver_e driver, DRV8313_timing_t* timing)
{
    switch (driver)
    {
    case DRV8313_DRIVER_LEFT:

        TIM3->CCR2 = timing->a;
        TIM12->CCR2 = timing->b;
        TIM12->CCR1 = timing->c;

        break;

    case DRV8313_DRIVER_RIGHT:

        TIM1->CCR4 = timing->a;
        TIM1->CCR2 = timing->b;
        TIM3->CCR4 = timing->c;

        break;
    }
}
