#ifndef INCLUDE_DRV8313_H
#define INCLUDE_DRV8313_H

#include <cstdint>

struct DRV8313_timing_t
{
    uint16_t a;
    uint16_t b;
    uint16_t c;
};

enum DRV8313_driver_e
{
    DRV8313_DRIVER_LEFT, DRV8313_DRIVER_RIGHT
};

void DRV8313_enable(DRV8313_driver_e driver, bool enable);

void DRV8313_setTiming(DRV8313_driver_e driver, DRV8313_timing_t* timing);

#endif
