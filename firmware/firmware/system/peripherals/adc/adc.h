/*
 * adc.h
 *
 *  Created on: May 12, 2020
 *      Author: mateusz.adamczyk
 */

#ifndef FIRMWARE_SYSTEM_PERIPHERALS_ADC_ADC_H_
#define FIRMWARE_SYSTEM_PERIPHERALS_ADC_ADC_H_

#include "system.h"

extern volatile uint16_t adcVal;



#endif /* FIRMWARE_SYSTEM_PERIPHERALS_ADC_ADC_H_ */
