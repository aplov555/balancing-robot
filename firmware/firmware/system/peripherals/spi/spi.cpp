#include "spi.h"

#include "system.h"

uint8_t SPI_sendReadByte(uint8_t data)
{
    while (!(SPI1->SR & SPI_SR_TXE))
        ;
    *(volatile uint8_t*) &SPI1->DR = data;

    while (!(SPI1->SR & SPI_SR_RXNE))
        ;
    return SPI1->DR;
}

void SPI_read(uint8_t* buffer, uint16_t bufferLen)
{
    for (uint16_t q = 0; q < bufferLen; ++q)
    {
        buffer[q] = SPI_sendReadByte(0);
    }
}
void SPI_send(uint8_t* buffer, uint16_t bufferLen)
{
    for (uint16_t q = 0; q < bufferLen; ++q)
    {
        SPI_sendReadByte(buffer[q]);
    }
}

void SPI_chipSelect(bool select)
{
    if (select)
        GPIOA->BSRR = GPIO_BSRR_BR_4;
    else
        GPIOA->BSRR = GPIO_BSRR_BS_4;
}
