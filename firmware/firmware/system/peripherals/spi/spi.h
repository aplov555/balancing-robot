#ifndef SPI_H
#define SPI_H

#include <cstdint>

uint8_t SPI_sendReadByte(uint8_t data);

void SPI_read(uint8_t* buffer, uint16_t bufferLen);
void SPI_send(uint8_t* buffer, uint16_t bufferLen);

void SPI_chipSelect(bool select);

#endif
