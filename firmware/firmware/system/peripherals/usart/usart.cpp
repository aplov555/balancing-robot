#include "usart.h"

#include "system.h"

bool USART_sendPacket(uint8_t* buff, uint16_t len)
{
    if (DMA1->HISR & DMA_HISR_TCIF4)
    {
        DMA1->HIFCR = DMA1->HISR;

        DMA1_Stream4->NDTR = len;
        DMA1_Stream4->M0AR = (uint32_t) buff;
        DMA1_Stream4->CR |= DMA_SxCR_EN;

        return true;
    }

    return false;
}
