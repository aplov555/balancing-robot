#ifndef INCLUDE_USART_H
#define INCLUDE_USART_H

#include <cstdint>

bool USART_sendPacket(uint8_t* buff, uint16_t len);

#endif
