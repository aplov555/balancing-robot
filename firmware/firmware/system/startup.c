#include <stddef.h>

extern void *_estack;

void Reset_Handler();
void Default_Handler();

void __attribute__((weak, naked)) NMI_Handler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) HardFault_Handler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) MemManage_Handler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) BusFault_Handler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) UsageFault_Handler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) SVC_Handler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DebugMon_Handler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) PendSV_Handler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) SysTick_Handler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) WWDG_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) PVD_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TAMP_STAMP_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) RTC_WKUP_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) FLASH_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) RCC_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) EXTI0_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) EXTI1_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) EXTI2_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) EXTI3_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) EXTI4_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA1_Stream0_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA1_Stream1_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA1_Stream2_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA1_Stream3_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA1_Stream4_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA1_Stream5_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA1_Stream6_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) ADC_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) CAN1_TX_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) CAN1_RX0_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) CAN1_RX1_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) CAN1_SCE_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) EXTI9_5_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM1_BRK_TIM9_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM1_UP_TIM10_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM1_TRG_COM_TIM11_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM1_CC_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM2_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM3_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM4_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) I2C1_EV_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) I2C1_ER_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) I2C2_EV_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) I2C2_ER_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) SPI1_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) SPI2_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) USART1_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) USART2_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) USART3_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) EXTI15_10_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) RTC_Alarm_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) OTG_FS_WKUP_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM8_BRK_TIM12_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM8_UP_TIM13_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM8_TRG_COM_TIM14_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM8_CC_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA1_Stream7_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) FMC_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) SDMMC1_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM5_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) SPI3_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) UART4_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) UART5_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM6_DAC_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) TIM7_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA2_Stream0_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA2_Stream1_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA2_Stream2_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA2_Stream3_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA2_Stream4_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) OTG_FS_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA2_Stream5_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA2_Stream6_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) DMA2_Stream7_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) USART6_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) I2C3_EV_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) I2C3_ER_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) OTG_HS_EP1_OUT_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) OTG_HS_EP1_IN_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) OTG_HS_WKUP_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) OTG_HS_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) RNG_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) FPU_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) UART7_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) UART8_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) SPI4_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) SPI5_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) SAI1_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) SAI2_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) QUADSPI_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) LPTIM1_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}
void __attribute__((weak, naked)) SDMMC2_IRQHandler() 
{
    __asm("bkpt 255");
    __asm("bx lr");
}

void * g_pfnVectors[0x78] __attribute__((section(".isr_vector"), used)) = 
{
    &_estack,
    &Reset_Handler,
    &NMI_Handler,
    &HardFault_Handler,
    &MemManage_Handler,
    &BusFault_Handler,
    &UsageFault_Handler,
    NULL,
    NULL,
    NULL,
    NULL,
    &SVC_Handler,
    &DebugMon_Handler,
    NULL,
    &PendSV_Handler,
    &SysTick_Handler,
    &WWDG_IRQHandler,
    &PVD_IRQHandler,
    &TAMP_STAMP_IRQHandler,
    &RTC_WKUP_IRQHandler,
    &FLASH_IRQHandler,
    &RCC_IRQHandler,
    &EXTI0_IRQHandler,
    &EXTI1_IRQHandler,
    &EXTI2_IRQHandler,
    &EXTI3_IRQHandler,
    &EXTI4_IRQHandler,
    &DMA1_Stream0_IRQHandler,
    &DMA1_Stream1_IRQHandler,
    &DMA1_Stream2_IRQHandler,
    &DMA1_Stream3_IRQHandler,
    &DMA1_Stream4_IRQHandler,
    &DMA1_Stream5_IRQHandler,
    &DMA1_Stream6_IRQHandler,
    &ADC_IRQHandler,
    &CAN1_TX_IRQHandler,
    &CAN1_RX0_IRQHandler,
    &CAN1_RX1_IRQHandler,
    &CAN1_SCE_IRQHandler,
    &EXTI9_5_IRQHandler,
    &TIM1_BRK_TIM9_IRQHandler,
    &TIM1_UP_TIM10_IRQHandler,
    &TIM1_TRG_COM_TIM11_IRQHandler,
    &TIM1_CC_IRQHandler,
    &TIM2_IRQHandler,
    &TIM3_IRQHandler,
    &TIM4_IRQHandler,
    &I2C1_EV_IRQHandler,
    &I2C1_ER_IRQHandler,
    &I2C2_EV_IRQHandler,
    &I2C2_ER_IRQHandler,
    &SPI1_IRQHandler,
    &SPI2_IRQHandler,
    &USART1_IRQHandler,
    &USART2_IRQHandler,
    &USART3_IRQHandler,
    &EXTI15_10_IRQHandler,
    &RTC_Alarm_IRQHandler,
    &OTG_FS_WKUP_IRQHandler,
    &TIM8_BRK_TIM12_IRQHandler,
    &TIM8_UP_TIM13_IRQHandler,
    &TIM8_TRG_COM_TIM14_IRQHandler,
    &TIM8_CC_IRQHandler,
    &DMA1_Stream7_IRQHandler,
    &FMC_IRQHandler,
    &SDMMC1_IRQHandler,
    &TIM5_IRQHandler,
    &SPI3_IRQHandler,
    &UART4_IRQHandler,
    &UART5_IRQHandler,
    &TIM6_DAC_IRQHandler,
    &TIM7_IRQHandler,
    &DMA2_Stream0_IRQHandler,
    &DMA2_Stream1_IRQHandler,
    &DMA2_Stream2_IRQHandler,
    &DMA2_Stream3_IRQHandler,
    &DMA2_Stream4_IRQHandler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    &OTG_FS_IRQHandler,
    &DMA2_Stream5_IRQHandler,
    &DMA2_Stream6_IRQHandler,
    &DMA2_Stream7_IRQHandler,
    &USART6_IRQHandler,
    &I2C3_EV_IRQHandler,
    &I2C3_ER_IRQHandler,
    &OTG_HS_EP1_OUT_IRQHandler,
    &OTG_HS_EP1_IN_IRQHandler,
    &OTG_HS_WKUP_IRQHandler,
    &OTG_HS_IRQHandler,
    NULL,
    NULL,
    &RNG_IRQHandler,
    &FPU_IRQHandler,
    &UART7_IRQHandler,
    &UART8_IRQHandler,
    &SPI4_IRQHandler,
    &SPI5_IRQHandler,
    NULL,
    &SAI1_IRQHandler,
    NULL,
    NULL,
    NULL,
    &SAI2_IRQHandler,
    &QUADSPI_IRQHandler,
    &LPTIM1_IRQHandler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    &SDMMC2_IRQHandler,
};

#include "system.h"

void __libc_init_array();
int main();

extern void *_sidata, *_sdata, *_edata;
extern void *_sbss, *_ebss;

void __attribute__((naked, noreturn)) Reset_Handler()
{
    void **pSource, **pDest;
    for (pSource = &_sidata, pDest = &_sdata; pDest != &_edata; pSource++, pDest++)
        *pDest = *pSource;

    for (pDest = &_sbss; pDest != &_ebss; pDest++)
        *pDest = 0;

    __libc_init_array();

    system_init_FPU();
    system_init_vectorTable();
    system_init_systemClock();
    system_init_clock();
    system_init_GPIO();
    system_init_timer();
    system_init_DMA();
    system_init_SPI();
    system_init_USART();
    system_init_ADC();
    system_init_sysTick();

    (void)main();
}

void __attribute__((naked, noreturn)) Default_Handler()
{
    while (1) ;
}
