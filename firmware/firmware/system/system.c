#include "system.h"

#include "stm32f722xx.h"


void system_init_FPU(void)
{
    SCB->CPACR  |=  ((3UL << 10 * 2) | (3UL << 11 * 2));  // set CP10 and CP11 Full Access
}
void system_init_systemClock(void)
{
    RCC->CR	    |=  RCC_CR_HSEON;
    while(!(RCC->CR & RCC_CR_HSERDY));

    RCC->PLLCFGR =  (12 << RCC_PLLCFGR_PLLM_Pos) |
                    (216 << RCC_PLLCFGR_PLLN_Pos) |
                    (0 << RCC_PLLCFGR_PLLP_Pos) |
                    (5 << RCC_PLLCFGR_PLLQ_Pos) |
                    (1 << 29) | // Reserved
                    RCC_PLLCFGR_PLLSRC_HSE;
    RCC->CR     |=  RCC_CR_PLLON;

    FLASH->ACR  =   FLASH_ACR_LATENCY_6WS;
    while ((FLASH->ACR & FLASH_ACR_LATENCY) != FLASH_ACR_LATENCY_6WS) ;

    RCC->CFGR   |=  RCC_CFGR_PPRE1_DIV4 |
                    RCC_CFGR_PPRE2_DIV4;

    while (!(RCC->CR & RCC_CR_PLLRDY)) ;
    RCC->CFGR |= RCC_CFGR_SW_PLL;
}

void system_init_vectorTable(void)
{
    SCB->VTOR   =   FLASH_BASE;
}

void system_init_clock(void)
{
    RCC->AHB1ENR    |=  RCC_AHB1ENR_GPIOAEN |
                        RCC_AHB1ENR_GPIOBEN |
                        RCC_AHB1ENR_GPIOCEN |
                        RCC_AHB1ENR_GPIOAEN |
                        RCC_AHB1ENR_DMA1EN |
                        RCC_AHB1ENR_DMA2EN;

    RCC->APB1ENR    |=  RCC_APB1ENR_TIM2EN |
                        RCC_APB1ENR_TIM3EN |
                        RCC_APB1ENR_TIM7EN |
                        RCC_APB1ENR_TIM12EN |
                        RCC_APB1ENR_TIM14EN |
                        RCC_APB1ENR_UART4EN;

    RCC->APB2ENR    |=  RCC_APB2ENR_TIM1EN |
                        RCC_APB2ENR_TIM9EN |
                        RCC_APB2ENR_SPI1EN |
                        RCC_APB2ENR_ADC1EN;

    __DSB();
}

void system_init_GPIO(void)
{
    GPIOA->MODER    =   GPIO_MODER_MODER0_1 |
                        GPIO_MODER_MODER1_1 |
                        GPIO_MODER_MODER2_0 |
                        GPIO_MODER_MODER3_1 |
                        GPIO_MODER_MODER4_0 |
                        GPIO_MODER_MODER5_1 |
                        GPIO_MODER_MODER6_1 |
                        GPIO_MODER_MODER7_1 |
                        GPIO_MODER_MODER8_0 |
                        GPIO_MODER_MODER9_1 |
                        GPIO_MODER_MODER10_0 |
                        GPIO_MODER_MODER11_1 |
                        GPIO_MODER_MODER12_0 |
                        GPIO_MODER_MODER13_1 |
                        GPIO_MODER_MODER14_1 |
                        GPIO_MODER_MODER15_1;

    GPIOB->MODER    =   GPIO_MODER_MODER15_1 |
                        GPIO_MODER_MODER14_1 |
                        GPIO_MODER_MODER13_0 |
                        GPIO_MODER_MODER12_0 |
                        GPIO_MODER_MODER11_0 |
                        GPIO_MODER_MODER8_0 |
                        GPIO_MODER_MODER7_0 |
                        GPIO_MODER_MODER6_0 |
                        GPIO_MODER_MODER3_1 |
                        GPIO_MODER_MODER2_0 |
                        GPIO_MODER_MODER1_0 |
                        GPIO_MODER_MODER0_0;

    GPIOC->MODER    =   GPIO_MODER_MODER11_0 |
                        GPIO_MODER_MODER10_0 |
                        GPIO_MODER_MODER9_1 |
                        GPIO_MODER_MODER8_0 |
                        GPIO_MODER_MODER7_1 |
                        GPIO_MODER_MODER6_0 |
                        GPIO_MODER_MODER1;

    GPIOA->AFR[0]   =   GPIO_AFRL_AFRL0_3 |
                        GPIO_AFRL_AFRL1_3 |
                        GPIO_AFRL_AFRL3_0 | GPIO_AFRL_AFRL3_1 |
                        GPIO_AFRL_AFRL5_0 | GPIO_AFRL_AFRL5_2 |
                        GPIO_AFRL_AFRL6_0 | GPIO_AFRL_AFRL6_2 |
                        GPIO_AFRL_AFRL7_0 | GPIO_AFRL_AFRL7_2;
    GPIOA->AFR[1]   =   GPIO_AFRH_AFRH1_0 |
                        GPIO_AFRH_AFRH3_0;

    GPIOB->AFR[0]   =   GPIO_AFRL_AFRL3_0;
    GPIOB->AFR[1]   =   GPIO_AFRH_AFRH7_3 | GPIO_AFRH_AFRH7_0 |
                        GPIO_AFRH_AFRH6_3 | GPIO_AFRH_AFRH6_0;

    GPIOC->AFR[0]   =   GPIO_AFRL_AFRL7_1;
    GPIOC->AFR[1]   =   GPIO_AFRH_AFRH1_1;

    GPIOC->PUPDR    =   GPIO_PUPDR_PUPDR4_0;
}

void system_init_timer(void)
{
    TIM1->CCMR1 =   TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_2;
    TIM1->CCMR2 =   TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC4M_2;
    TIM1->CCER  =   TIM_CCER_CC2E |
                    TIM_CCER_CC4E;
    TIM1->BDTR  =   TIM_BDTR_MOE;
    TIM1->CR1   =   TIM_CR1_ARPE;
    TIM1->PSC   =   0;
    TIM1->ARR   =   SYSTEM_MOTORS_PWM_MAX;

    TIM2->CCMR1 =   TIM_CCMR1_CC1S_0 |
                    TIM_CCMR1_CC2S_1;
    TIM2->CCER  =   TIM_CCER_CC2P |
                    TIM_CCER_CC1E |
                    TIM_CCER_CC2E;
    TIM2->SMCR  =   TIM_SMCR_TS_2 |
                    TIM_SMCR_TS_0 |
                    TIM_SMCR_SMS_2;
    TIM2->PSC   =   SYSTEM_ENCODER_TIMER_PRESCALER;
    TIM2->CR2   =   TIM_CR2_TI1S;

    TIM3->CCMR1 =   TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_2;
    TIM3->CCMR2 =   TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC4M_2; 
    TIM3->CCER  =   TIM_CCER_CC2E |
                    TIM_CCER_CC4E;
    TIM3->BDTR  =   TIM_BDTR_MOE;
    TIM3->CR1   =   TIM_CR1_ARPE;
    TIM3->PSC   =   0;
    TIM3->ARR   =   SYSTEM_MOTORS_PWM_MAX;

    TIM7->PSC   =   SYSTEM_SYNC_LOOP_LOW_PSC;
    TIM7->ARR   =   SYSTEM_SYNC_LOOP_LOW_ARR;
    TIM7->DIER  =   TIM_DIER_UIE;

    TIM9->CCMR1 =   TIM_CCMR1_CC1S_1 |
                    TIM_CCMR1_CC2S_0;
    TIM9->CCER  =   TIM_CCER_CC1P |
                    TIM_CCER_CC1E |
                    TIM_CCER_CC2E;
    TIM9->SMCR  =   TIM_SMCR_TS_2 |
                    TIM_SMCR_TS_1 |
                    TIM_SMCR_SMS_2;
    TIM9->PSC   =   SYSTEM_ENCODER_TIMER_PRESCALER;
    
    TIM12->CCMR1=   TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2 |
                    TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_2;
    TIM12->CCER =   TIM_CCER_CC1E |
                    TIM_CCER_CC2E;
    TIM12->CR1  =   TIM_CR1_ARPE;
    TIM12->PSC  =   0;
    TIM12->ARR  =   SYSTEM_MOTORS_PWM_MAX;

    TIM14->PSC  =   SYSTEM_SYNC_LOOP_HIGH_PSC;
    TIM14->ARR  =   SYSTEM_SYNC_LOOP_HIGH_ARR;
    TIM14->DIER =   TIM_DIER_UIE;

    TIM1->CR1   |=  TIM_CR1_CEN;
    TIM2->CR1   |=  TIM_CR1_CEN;
    TIM3->CR1   |=  TIM_CR1_CEN;
    TIM7->CR1	|=	TIM_CR1_CEN;
    TIM9->CR1   |=  TIM_CR1_CEN;
    TIM12->CR1  |=  TIM_CR1_CEN;
    TIM14->CR1  |=  TIM_CR1_CEN;

    NVIC_SetPriority(TIM7_IRQn, SYSTEM_SYNC_LOOP_LOW_PRIO);
    NVIC_SetPriority(TIM8_TRG_COM_TIM14_IRQn, SYSTEM_SYNC_LOOP_HIGH_PRIO);

    NVIC_EnableIRQ(TIM7_IRQn);
    NVIC_EnableIRQ(TIM8_TRG_COM_TIM14_IRQn);
}

void system_init_DMA(void)
{
    char buff = ' ';

    DMA1_Stream4->NDTR  =   1;
    DMA1_Stream4->M0AR  =   (uint32_t)&buff;
    DMA1_Stream4->PAR   =   (uint32_t)&UART4->TDR;
    DMA1_Stream4->CR    =   DMA_SxCR_CHSEL_2 |
                            DMA_SxCR_MINC |
                            DMA_SxCR_DIR_0 |
                            DMA_SxCR_EN;

    DMA2_Stream0->M0AR  =   (uint32_t)&adcVal;
    DMA2_Stream0->PAR   =   (uint32_t)&ADC1->DR;
    DMA2_Stream0->NDTR  =   1;
    DMA2_Stream0->CR    =   DMA_SxCR_MSIZE_0 |
                            DMA_SxCR_PSIZE_0 |
                            DMA_SxCR_MINC |
                            DMA_SxCR_CIRC |
                            DMA_SxCR_EN;
}

void system_init_SPI(void)
{
    SPI1->CR2   |=  SPI_CR2_FRXTH |
                    SPI_CR2_NSSP;
    SPI1->CR1   =   SPI_CR1_BR_1 | SPI_CR1_BR_0 |
                    SPI_CR1_MSTR |
                    SPI_CR1_SSI |
                    SPI_CR1_SSM |
                    SPI_CR1_SPE;
}

void system_init_USART(void)
{
    UART4->BRR  =   SYSTEM_SYSTEM_CLOCK_FREQ / (4*SYSTEM_UART4_BAUD_RATE);
    UART4->CR3  =   USART_CR3_DMAT;
    UART4->CR1  =   USART_CR1_UE |
                    USART_CR1_RE |
                    USART_CR1_TE |
                    USART_CR1_RXNEIE;

    NVIC_EnableIRQ(UART4_IRQn);
}

void system_init_ADC(void)
{
    ADC->CCR    =   ADC_CCR_ADCPRE_0;
    ADC1->SQR3  =   ADC_SQR3_SQ1_3 | ADC_SQR3_SQ1_1 | ADC_SQR3_SQ1_0;
    ADC1->CR2   =   ADC_CR2_ADON |
                    ADC_CR2_CONT |
                    ADC_CR2_DDS |
                    ADC_CR2_DMA;
    ADC1->CR2   |=  ADC_CR2_SWSTART;
}

void system_init_sysTick(void)
{
    SysTick_Config(SYSTEM_SYSTEM_CLOCK_FREQ/1000);
}
