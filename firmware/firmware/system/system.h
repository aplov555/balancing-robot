#ifndef INCLUDE_SYSTEM_H
#define INCLUDE_SYSTEM_H

/* BEGIN SYSTEM CONFIG */
#define SYSTEM_SYSTEM_CLOCK_FREQ        216000000
#define SYSTEM_MOTORS_PWM_MAX           (4500-1)    // 216 MHz / 2 / 4500 = 24 kHz
#define SYSTEM_ENCODER_TIMER_PRESCALER  (10-1)      /* timer clock freq - 216 MHz / 4 / 10 = 5.4 MHz
                                                       PWM encoder output freq - 1 kHz 
                                                       timer max cnt - 65535
                                                       5.4 MHz / 1kHZ < 65535*/
#define SYSTEM_UART4_BAUD_RATE          460800

/*  END SYSTEM CONFIG */

#include "stm32f722xx.h"
#include "system_syncLoop.h"
#include "adc.h"

void system_init_FPU(void);
void system_init_systemClock(void);
void system_init_vectorTable(void);
void system_init_clock(void);
void system_init_GPIO(void);
void system_init_timer(void);
void system_init_DMA(void);
void system_init_SPI(void);
void system_init_USART(void);
void system_init_ADC(void);
void system_init_sysTick(void);

uint32_t system_get_systemTick(void);

void system_delayms(uint32_t ms);

#endif
