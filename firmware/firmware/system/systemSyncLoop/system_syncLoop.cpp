#include "system_syncLoop.h"

static volatile uint8_t system_syncFunHighCnt;
static volatile uint8_t system_syncFunLowCnt;

sytem_syncFunPointer syncHighFun[SYSTEM_SYNC_LOOP_HIGH_MAX_FUNS];
sytem_syncFunPointer syncLowFun[SYSTEM_SYNC_LOOP_LOW_MAX_FUNS];

void system_syncFun_addFun(sytem_syncFunPointer syncFunPointer,
        System_syncLoop_e syncLoop)
{
    switch (syncLoop)
    {
    case SYSTEM_SYNC_LOOP_HIGH:

        syncHighFun[system_syncFunHighCnt++] = syncFunPointer;

        break;

    case SYSTEM_SYNC_LOOP_LOW:

        syncLowFun[system_syncFunLowCnt++] = syncFunPointer;

        break;
    }

}

extern "C"
{
void __attribute__((interrupt)) TIM8_TRG_COM_TIM14_IRQHandler(void)
{
    if (TIM14->SR & TIM_SR_UIF)
    {
        TIM14->SR &= ~TIM_SR_UIF;

        for (uint32_t q = 0; q < system_syncFunHighCnt; ++q)
        {
            syncHighFun[q]();
        }
    }
}

void __attribute__((interrupt)) TIM7_IRQHandler(void)
{
    if (TIM7->SR & TIM_SR_UIF)
    {
        TIM7->SR &= ~TIM_SR_UIF;

        for (uint32_t q = 0; q < system_syncFunLowCnt; ++q)
        {
            syncLowFun[q]();
        }
    }
}
}
