#ifndef INCLUDE_SYSTEM_SYNC_LOOP_H
#define INCLUDE_SYSTEM_SYNC_LOOP_H

#define SYSTEM_SYNC_LOOP_HIGH_MAX_FUNS  10
#define SYSTEM_SYNC_LOOP_HIGH_PSC       (2000-1)
#define SYSTEM_SYNC_LOOP_HIGH_ARR       (54-1)
#define SYSTEM_SYNC_LOOP_HIGH_PRIO      0

#define SYSTEM_SYNC_LOOP_LOW_MAX_FUNS   10
#define SYSTEM_SYNC_LOOP_LOW_PSC        (4000-1)
#define SYSTEM_SYNC_LOOP_LOW_ARR        (54-1)
#define SYSTEM_SYNC_LOOP_LOW_PRIO       1

#include "system.h"

typedef enum System_syncLoop_e
{
    SYSTEM_SYNC_LOOP_HIGH, SYSTEM_SYNC_LOOP_LOW
} System_syncLoop_e;

typedef void (*sytem_syncFunPointer)(void);

void system_syncFun_addFun(sytem_syncFunPointer syncFunPointer,
        System_syncLoop_e syncLoop);

#endif
