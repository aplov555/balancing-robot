#include "system.h"

static volatile uint32_t systemTick;

uint32_t system_get_systemTick(void)
{
    return systemTick;
}

void system_delayms(uint32_t ms)
{
    uint32_t tick = systemTick;

    while (systemTick - tick <= ms) ;
}

extern "C"
{
    void __attribute__((interrupt)) SysTick_Handler(void)
    {
        ++systemTick;
    }
}
